import css from '../public/css/astro.core.css';

const astroCSS = document.createElement('dom-module');
astroCSS.innerHTML = `<template>
<style>
${css}
</style>
</template>`;
astroCSS.register('astro-css');
