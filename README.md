# GRM Sample Apps: Equipment Manager

## Description
A core requirement of GRM is to ensure that the equipment on the ground responsible for communicating with satellites is operational and available. This equipment includes hardware such as antennas, processors and software systems that all must interact with one another during a satellite contact. These resources are often shared amongst multiple operations, so if a piece of equipment is not available, it can affect multiple missions. As such, it is critical for operators to quickly identify equipment in need of attention and schedule maintenance to get it back up and running as quickly as possible.

The GRM Equipment Manager app is designed to support this capability by consolidating information related to all ground equipment in one place. The app's home page, the Inoperable Equipment page, immediately provides operators with a list of all inoperable ground equipment. From there, operators can navigate to the Equipment Details page to view the full details of specific inoperable equipment and take action such as schedule a maintenance job. 

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)*
2. [Node.js](https://nodejs.org/)
3. [npm](https://www.npmjs.com/get-npm)
4. [Polymer](https://www.polymer-project.org/)
5. [EGS Web Socket Server](https://bitbucket.org/rocketcom/egs-socket-server/src/master/) (optional)**
6. [EGS Web Services Server](https://bitbucket.org/rocketcom/egs-data-services/src/master/) (optional)**

\* It may be possible to run on Windows but has not been thoroughly tested and is not recommended.
\*\* You only need to run your own Web Socket and Web Services servers if you are behind a firewall that prevents you from accessing the ones we provide at <wss://sockets.astrouxds.com> and <https://services.astrouxds.com> respectively.


## Getting Started
### Clone this repository

`git clone git@bitbucket.org:rocketcom/grm-sample-apps-equipment.git`

### Install the Polymer CLI

`npm i -g polymer-cli`

### Install NPM modules for this project

`npm i`

### Create a symbolic link to the appropriate file in the `config` directory.

We recommend linking to config.local.json:

`ln -s config.local.json config.json`

However, if you are running your own Web Socket and Web Services servers (see above) you will need to create your own config file that points to your own servers. Use one of the existing config files as a template and substitute the appropriate URLs.

### For local development and testing

`npm run start`

### To build for production

`npm run build`

### Load the application(s) in your browser

Assuming you've linked your config file to config.local.json:

<http://localhost:8001>

Otherwise, see values from your config file and/or the output from `npm run start`